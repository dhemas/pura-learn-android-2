package com.dhemas.app.tabapp;

import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.dhemas.app.tabapp.Adapter.AdapterViewPager;
import com.dhemas.app.tabapp.Fragment.FragmentA;
import com.dhemas.app.tabapp.Fragment.FragmentB;
import com.dhemas.app.tabapp.Fragment.FragmentC;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView txt_title;

    public static AdapterViewPager adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();
    }

    private void initUI() {
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        txt_title = (TextView) findViewById(R.id.txt_title);

        adapter = new AdapterViewPager(getSupportFragmentManager());
        adapter.addFragment(new FragmentA(), "Fragment A");
        adapter.addFragment(new FragmentB(), "Fragment B");
        adapter.addFragment(new FragmentC(), "Fragment C");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        //set font lalala yeyeye
        Typeface typeface = Typeface.createFromAsset(getAssets(), "waltographUI.ttf");
        txt_title.setTypeface(typeface);
    }
}