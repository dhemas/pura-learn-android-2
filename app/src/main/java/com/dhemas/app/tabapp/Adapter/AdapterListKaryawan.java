package com.dhemas.app.tabapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dhemas.app.tabapp.DialogActionKaryawan;
import com.dhemas.app.tabapp.Karyawan;
import com.dhemas.app.tabapp.R;

import java.util.ArrayList;

/**
 * TabApp by dhemas
 * created on: 05/04/2017
 */

public class AdapterListKaryawan extends BaseAdapter {

    private ArrayList<Karyawan> listKaryawan;
    private LayoutInflater inflater;

    public AdapterListKaryawan(ArrayList<Karyawan> listKaryawan, Context context) {
        this.listKaryawan = listKaryawan;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listKaryawan.size();
    }

    @Override
    public Object getItem(int i) {
        return listKaryawan.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rowView = inflater.inflate(R.layout.layout_list_karyawan, viewGroup, false);

        TextView txt_id_karyawan = (TextView) rowView.findViewById(R.id.txt_id_karyawan);
        TextView txt_nama_karyawan = (TextView) rowView.findViewById(R.id.txt_nama_karyawan);
        TextView txt_alamat_karyawan = (TextView) rowView.findViewById(R.id.txt_alamat_karyawan);

        Karyawan karyawan = (Karyawan) getItem(i);
        txt_id_karyawan.setText(karyawan.ID_KARYAWAN);
        txt_nama_karyawan.setText(karyawan.NAMA);
        txt_alamat_karyawan.setText(karyawan.ALAMAT);

        return rowView;
    }
}
