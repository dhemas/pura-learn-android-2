package com.dhemas.app.tabapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.VolleyError;
import com.dhemas.app.tabapp.Fragment.FragmentA;
import com.dhemas.app.tabapp.Request.RequestDeleteKaryawan;

import java.util.HashMap;

import static com.dhemas.app.tabapp.Utility.U;

/**
 * TabApp by dhemas
 * created on: 06/04/2017
 */

public class DialogActionKaryawan implements MaterialDialog.SingleButtonCallback {

    private MaterialDialog dialog;
    private Context context;
    private String idKaryawan;

    public DialogActionKaryawan(Context context) {
        this.context = context;

        this.dialog = new MaterialDialog.Builder(context)
                .title("Action Required")
                .content("Please select your action")
                .positiveText("Delete")
                .negativeText("Update")
                .neutralText("Cancel")
                .onPositive(this)
                .onNegative(this)
                .onNeutral(this)
                .build();
    }

    public void setIDKaryawan(String id){
        this.idKaryawan = id;
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        if(which == DialogAction.POSITIVE){
            onPositive();
        }
        if(which == DialogAction.NEGATIVE){

        }
        if(which == DialogAction.NEUTRAL){

        }
    }

    private void onPositive() {

        HashMap<String, String> map = new HashMap<>();
        map.put("id_karyawan", idKaryawan);

        RequestDeleteKaryawan request = new RequestDeleteKaryawan(context, map) {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("--response", error.toString());
            }

            @Override
            public void onResponse(String response) {
                Log.i("--response", response);

                if(response.contains("success")){
                    U.fetchData(context);
                } else {

                }
            }
        };

        request.sendRequest();
    }

    //getter
    public MaterialDialog getDialog(){
        return this.dialog;
    }
}